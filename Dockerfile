# Set the base image for subsequent instructions
FROM ubuntu:20.04

ENV TZ=Mexico/General

RUN export LC_ALL=es_MX.UTF-8
RUN DEBIAN_FRONTEND=noninteractive
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

MAINTAINER Luis Yair Miranda Gonzalez <lyairmga@gmail.com>

# Update packages
RUN apt-get update

RUN apt-get upgrade -y

RUN apt-get install -y \
    sudo \
    autoconf \
    autogen \
    language-pack-es-base \
#    wget \
    zip \
    unzip \
    curl \
#    rsync \
    ssh \
    openssh-client \
    language-pack-es \
    git \
#    build-essential \
    apt-utils \
    software-properties-common \
    csvkit

RUN useradd -m docker && echo "docker:docker" | chpasswd && adduser docker sudo


# Other
RUN mkdir ~/.ssh
RUN touch ~/.ssh_config
